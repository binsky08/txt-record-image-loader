# TXT Record Image Loader

This project offers a small web application for TXT dns record based image file sharing.

[Example web instance](https://images.web2ju.de/)

## Usage
- Access an image, shared with you
  - If somebody shared a fqdn with you to access an image from TXT dns record data, open the web application and paste it into the primary input field
  - When pressing the "Load Image" button, the shared image should appear shortly
- Share an own image
  - Open the web application and use the "Prepare own image" button, to get the preparation view
  - Select a custom image, that will be compressed and converted to webp format, to match the max content length of 64771 byte
  - If the compression process was successful the processed image will be shown
  - Use the copy button or just copy the image source data
  - Add a TXT dns record to your zone (maybe at your dns providers web interface)
  - Share your image using the recently created dns record fqdn like `https://images.web2ju.de/#share=my.fqdn.com`

## Background

The technical rdata field size limit (65535 byte), limits the dns record content length. Nice, ~65kb of "free" usable storage within a dns record.

The only usable record type to store completely custom data within, is TXT.

Strings in TXT records shouldn't be longer than 255 characters.

However, to get around this limitation, a TXT record is allowed to contain multiple strings, which should be concatenated together by the reading application (dns library).

Using this special technique, it is possible to store 64771 byte of content (+ padding `" "` between the short strings) in TXT records. (= 64771 ascii characters)

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode).
