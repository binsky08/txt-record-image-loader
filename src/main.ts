import './app.css'
import App from './App.svelte'

const app = new App({
    target: document.getElementById('app') ?? new HTMLElement(),
})

export default app

document.addEventListener("DOMContentLoaded", () => {
    const gitlabLink = document.getElementById('gitlabLink');
    if (gitlabLink) {
        gitlabLink.style.display = 'block';
    }
});
