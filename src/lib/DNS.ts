import { combineTXT, query } from "dns-query";

export class DNS {
    public static async fetchTXTContent(name: string) {
        const { answers, rcode } = await query(
            { question: { type: 'TXT', name: name } },
            { endpoints: ['1.1.1.1'] }
        )

        if (answers) {
            // @ts-ignore
            const txt = answers.map(answer => combineTXT(answer.data)) as unknown as string;
            try {
                window.atob(txt);
                // if we are here, it should be encoded within a valid, complete base64 string
                return 'data:image/png;base64,' + txt;
            } catch (e) {
                // something failed, seems to not be base64
                return txt;
            }
        }
        return '';
    }
}
