export const copyToClipboard = (value: string, fieldTitle: string) => {
    if (navigator.clipboard) {
        navigator.clipboard.writeText(value).then(
            () => {
                /* Resolved - text copied to clipboard successfully */
                //console.log(fieldTitle + ' copied to clipboard');
                console.log(fieldTitle + ' copied to clipboard');
            },
            () => {
                /* Rejected - text failed to copy to the clipboard */
                //console.error('Failed to copy ' + fieldTitle);
                console.error('Failed to copy ' + fieldTitle);
            }
        );
    } else {
        console.error('Failed to copy ' + fieldTitle);
    }
};
